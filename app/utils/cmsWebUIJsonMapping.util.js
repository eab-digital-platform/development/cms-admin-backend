import * as _ from 'lodash';

const cmsJsonToWebUIJson = (name, cmsJson) => {
  const webUIJson = {};
  // Basic info
  webUIJson.id = name;
  webUIJson.type = cmsJson.type;
  webUIJson.pageOrComponent = cmsJson.pageOrComponent;
  // Property info
  let keysOfProperty = {};
  if (cmsJson.property.items) {
    keysOfProperty = Object.keys(_.omit(cmsJson.property, 'items'));
  } else {
    keysOfProperty = Object.keys(cmsJson.property);
  }
  _.forEach(keysOfProperty, (key) => {
    webUIJson[`${key}`] = cmsJson.property[`${key}`];
  });
  // System info
  webUIJson.system = cmsJson.system;
  return webUIJson;
};

let rootItems = [];
const loopItemsOnCmsJson = (json) => {
  if (json.property && json.property.items) {
    _.forEach(json.property.items, (item) => {
      rootItems.push(cmsJsonToWebUIJson(item.type, item));
      if (item.property && item.property.items) {
        loopItemsOnCmsJson(item.property.items);
      }
    });
  }
};

const convertAllCmsJsonToWebUIJson = (cmsJson) => {
  rootItems = [];
  const webUIJson = cmsJsonToWebUIJson(cmsJson.type, cmsJson);
  webUIJson.items = [];
  loopItemsOnCmsJson(cmsJson);
  return webUIJson;
};

export default convertAllCmsJsonToWebUIJson;
