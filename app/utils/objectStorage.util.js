import * as minio from 'minio';
import * as _ from 'lodash';
import axios from 'axios';

import getConfig from './config.util';
import log4jUtil from './log4j.util';

const getFileUrlThroughFileApi = async (
  fileApiUrl,
  fileServiceName,
  bucketName,
  fileId,
) => {
  const url = await axios.request({
    baseURL: fileApiUrl,
    url: `${fileApiUrl}/file-api/${fileServiceName}/${bucketName}/${fileId}`,
    method: 'GET',
  }).then(result => result).catch(error => ({ error }));
  return url.data;
};

const connectObjectStorage = () => {
  const minioClient = new minio.Client({
    endPoint: getConfig('minio.endPoint'),
    port: getConfig('minio.port'),
    useSSL: getConfig('minio.useSSL'),
    accessKey: getConfig('minio.accessKey'),
    secretKey: getConfig('minio.secretKey'),
  });
    // For S3 in the future
    // var s3Client = new Minio.Client({
    //     endPoint:  's3.amazonaws.com',
    //     accessKey: 'YOUR-ACCESSKEYID',
    //     secretKey: 'YOUR-SECRETACCESSKEY'
    // })
  minioClient.listBuckets((err, result) => {
    if (err) {
      log4jUtil.log('error', JSON.stringify(err));
    } else {
      log4jUtil.log('info', `Connected to minio server: ${getConfig('minio.endPoint')}:${getConfig('minio.port')} [${_.map(result, ({ name }) => (name))}]`);
    }
  });
  return minioClient;
};

const getObjectUrl = async (bucketName, objectKey) => new Promise((resolve, reject) => {
  connectObjectStorage().presignedUrl('GET', bucketName, objectKey, 604800, (err, result) => {
    if (err) {
      reject(err);
    } else {
      resolve(result);
    }
  });
});

export default { connectObjectStorage, getObjectUrl, getFileUrlThroughFileApi };
