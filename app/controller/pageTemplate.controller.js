/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import PageTemplate from '../model/pageTemplate.model';
import log4jUtil from '../utils/log4j.util';
import objectStorageUtil from '../utils/objectStorage.util';

const getImageUrl = async (bucket, key) => new Promise((resolve) => {
  objectStorageUtil.getObjectUrl(bucket, key).then((result) => {
    resolve(result);
  });
});

const convertDisplayImageToImageUrl = async (item) => {
  let tempItem = _.cloneDeep(item);
  tempItem = _.omit(tempItem, 'displayImage');
  tempItem.imageUrl = await getImageUrl(item.displayImage.bucket, item.displayImage.key);
  return tempItem;
};

/**
 * @function
 * @description Get all page master data on pageMaster collection
 * @returns {array} Array of json files
 */
const getList = async () => {
  const result = await PageTemplate.find().lean();
  log4jUtil.log('info', `List result length:${result.length}`);
  const resultWithImageUrl = [];
  for (let i = 0; i < result.length; i += 1) {
    if (result[`${i}`].displayImage && result[`${i}`].displayImage.bucket && result[`${i}`].displayImage.key) {
      /* eslint-disable no-await-in-loop */
      const newItem = await convertDisplayImageToImageUrl(result[`${i}`]);
      resultWithImageUrl.push(newItem);
    } else {
      resultWithImageUrl.push(result[`${i}`]);
    }
  }
  return resultWithImageUrl;
};

const getPageTemplate = async (pageTemplateId) => {
  const pageTemplate = await PageTemplate.findById(pageTemplateId);
  return pageTemplate;
};

export default {
  getList,
  getPageTemplate,
};
