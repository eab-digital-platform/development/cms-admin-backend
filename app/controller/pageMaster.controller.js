/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import moment from 'moment';
import PageMaster from '../model/pageMaster.model';
import Project from '../model/project.model';
import Mapping from '../model/mapping.model';
import Organization from '../model/organization.model';
import ObjectMapping from '../model/objectMapping.model';
import log4jUtil from '../utils/log4j.util';
import objectStorageUtil from '../utils/objectStorage.util';
import config from '../../config/dev.json';

/**
 * @function
 * @description Create a page master json record
 * @param {object} pageMasterContent - page master content
 * @returns {string} page master id if success/ error message if fail
 */
const createPageMaster = async (pageMasterContent) => {
  const newPageMaster = new PageMaster(pageMasterContent);
  const result = await newPageMaster.save();
  const id = '_id';
  return result[`${id}`];
};

const createPageMasterByProjectId = async (projectId, pageMasterContent) => {
  const existingProject = await Project.findById(projectId);
  if (!existingProject) {
    return {
      error: 'Project not exists',
    };
  }
  const newPageMaster = new PageMaster(pageMasterContent);
  const result = await newPageMaster.save();
  const id = '_id';
  const newMapping = new Mapping({
    ownerLevel: 'pageMaster',
    ownerId: result[`${id}`],
    mappingLevel: 'project',
    mappingId: projectId,
  });
  await newMapping.save();
  return result[`${id}`];
};

const createPageMasterByOrganizationId = async (organizationId, pageMasterContent) => {
  const existingOrganization = await Organization.findById(organizationId);
  if (existingOrganization) {
    return {
      error: 'Organization not exists',
    };
  }
  const newPageMaster = new PageMaster(pageMasterContent);
  const result = await newPageMaster.save();
  const id = '_id';
  const newMapping = new Mapping({
    ownerLevel: 'pageMaster',
    ownerId: result[`${id}`],
    mappingLevel: 'organization',
    mappingId: organizationId,
  });
  await newMapping.save();
  return result[`${id}`];
};

/**
 * @function
 * @description Update a page master record
 * @param {string} pageMasterId - page master id
 * @param {object} newPageMasterContent - new page master content
 */
const updatePageMaster = async (pageMasterId, newPageMasterContent) => {
  const result = await PageMaster.findById(pageMasterId);
  if (result) {
    const newPageMaster = await PageMaster.findOneAndUpdate({
      _id: pageMasterId,
    }, newPageMasterContent);
    return newPageMaster[`${'_id'}`];
  }
  return '';
};

/**
 * @function
 * @description Find the page master json by ID and return
 * @param {string} pageMasterId - page master id in mongodb
 * @returns {string} Page master record
 */
const getPageMaster = async (pageMasterId) => {
  const result = await PageMaster.findById(pageMasterId);
  return result;
};

/**
 * @function
 * @description Get all page master data on pageMaster collection
 * @returns {array} Array of json files
 */
const getList = async () => {
  const results = await PageMaster.find().select('type displayName system description code level').sort({
    code: -1,
  }).lean();
  log4jUtil.log('info', `List result length:${results.length}`);
  _.forEach(results, (result, i) => {
    results[`${i}`].system.createdDate = moment(results[`${i}`].system.createdDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
    results[`${i}`].system.updatedDate = moment(results[`${i}`].system.updatedDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
  });
  return results;
};

/**
 * @function
 * @description Delete one of the page master record
 */
const deletePageMaster = async (pageMasterId) => {
  const result = await PageMaster.findById(pageMasterId);
  if (!result) {
    return { error: 'page not exists' };
  }
  await PageMaster.findOneAndDelete({
    _id: pageMasterId,
  });
  await Mapping.findOneAndDelete({
    ownerLevel: 'pageMaster',
    ownerId: pageMasterId,
  });
  return {};
};

const clonePageMaster = async (pageMasterId) => {
  const oldPageMaster = await PageMaster.findById(pageMasterId).lean();
  if (!oldPageMaster) {
    return { error: 'pageMaster not exists' };
  }
  const clonedPageMaster = _.cloneDeep(oldPageMaster);
  clonedPageMaster.code = `${oldPageMaster.code} Copy`;
  clonedPageMaster.system.createdDate = moment();
  clonedPageMaster.system.updatedDate = moment();
  const newPageMaster = new PageMaster(_.omit(clonedPageMaster, ['_id']));
  const newSavedPageMaster = await newPageMaster.save();
  const oldMapping = await Mapping.findOne({
    ownerId: pageMasterId,
  });
  const mapping = new Mapping({
    ownerLevel: 'pageMaster',
    ownerId: newSavedPageMaster[`${'_id'}`],
    mappingLevel: oldMapping.mappingLevel,
    mappingId: oldMapping.mappingId,
  });
  await mapping.save();
  return newSavedPageMaster[`${'_id'}`];
};

const movePage = async (ownerId, toLevel, toId) => {
  await Mapping.findOneAndUpdate({
    ownerLevel: 'pageMaster',
    ownerId,
  }, {
    ownerLevel: 'pageMaster',
    ownerId,
    mappingLevel: toLevel,
    mappingId: toId,
  });
  return {};
};

const getObjectInfo = async (pageMasterId, objectId) => {
  const pageMaster = await PageMaster.findById(pageMasterId);
  if (pageMaster) {
    const objectMapping = await ObjectMapping.findById(objectId);
    if (objectMapping.mappingLevel === 'pageMaster' && objectMapping.mappingId === pageMasterId) {
      const fileServiceName = config.fileApi.fileServiceName ? config.fileApi.fileServiceName : 'minio';
      const bucketName = config.fileApi.bucketName ? config.fileApi.bucketName : 'trans';
      const url = await objectStorageUtil.getFileUrlThroughFileApi(
        config.fileApi.url,
        fileServiceName,
        bucketName,
        objectId,
      );
      return {
        url,
        objectName: objectMapping.meta.originalname,
      };
    }
    return { error: 'Object id not exists' };
  }
  return { error: 'Page master id not exists' };
};

export default {
  createPageMaster,
  createPageMasterByProjectId,
  createPageMasterByOrganizationId,
  updatePageMaster,
  getPageMaster,
  deletePageMaster,
  getList,
  clonePageMaster,
  movePage,
  getObjectInfo,
};
