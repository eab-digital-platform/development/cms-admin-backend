/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import Mapping from '../model/mapping.model';
import PageMaster from '../model/pageMaster.model';
import Organization from '../model/organization.model';

const getPageIdsByOrganizationId = async (organizationId) => {
  const mappings = await Mapping.find({
    ownerLevel: 'pageMaster',
    mappingLevel: 'organization',
    mappingId: organizationId,
  }).lean();
  return _.map(mappings, 'ownerId');
};

const getPageDetail = async (pageId) => {
  const pageDetail = await PageMaster.findById(pageId);
  return pageDetail;
};

const getAllPageDetails = async (pageIds) => {
  const resultPromises = [];
  _.forEach(pageIds, (pageId) => {
    resultPromises.push(getPageDetail(pageId));
  });
  const results = await Promise.all(resultPromises);
  return results;
};

const getPagesByOrganizationId = async (organizationId) => {
  const pageIds = await getPageIdsByOrganizationId(organizationId);
  const results = await getAllPageDetails(pageIds);
  return results;
};

const getOrganization = async (organizationId) => {
  const result = await Organization.findById(organizationId);
  return result;
};

export default {
  getPagesByOrganizationId,
  getOrganization,
};
