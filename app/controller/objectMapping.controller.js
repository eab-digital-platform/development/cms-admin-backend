import fs from 'fs';
import request from 'request-promise';
import config from '../../config/dev.json';
import ObjectMapping from '../model/objectMapping.model';
import PageMaster from '../model/pageMaster.model';
import ComponentSet from '../model/componentSet.model';
import log4jUtil from '../utils/log4j.util';
import objectStorageUtil from '../utils/objectStorage.util';

const checkIfMappingIdExists = async (mappingLevel, mappingId) => {
  let result;
  if (mappingLevel === 'pageMaster') {
    result = await PageMaster.findById(mappingId);
  } else if (mappingLevel === 'componentSet') {
    result = await ComponentSet.findById(mappingId);
  }
  return !!result;
};

const createNewObjectMapping = async (
  fileServiceName,
  bucketName,
  meta,
  mappingLevel,
  mappingId,
) => {
  const objectMapping = new ObjectMapping({
    fileServiceName,
    bucketName,
    meta,
    mappingLevel,
    mappingId,
  });
  const newObjectResult = await objectMapping.save();
  return newObjectResult;
};

const uploadFileThroughFileApi = async (
  fileApiUrl,
  fileServiceName,
  bucketName,
  fileId,
  fileName,
  fileStream,
) => {
  const options = {
    method: 'PUT',
    url: `${fileApiUrl}/file-api/${fileServiceName}/${bucketName}/${fileId}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    formData: {
      file: {
        value: fileStream,
        options: {
          filename: fileName,
          contentType: null,
        },
      },
    },
  };
  log4jUtil.log('info', 'Call file api to upload');
  const result = await request(options).catch(error => ({ error: error.cause.message }));
  console.log('result:', result);
  if (result.error) {
    return result;
  }
  console.log('result:', result);
  return {};
};

const uploadFile = async (file, mappingLevel, mappingId) => {
  log4jUtil.log('info', `mappingLevel:${mappingLevel}`);
  log4jUtil.log('info', `mappingId:${mappingId}`);
  log4jUtil.log('info', `file: ${JSON.stringify(file)}`);
  const isMappingIdExists = await checkIfMappingIdExists(mappingLevel, mappingId);
  if (isMappingIdExists) {
    log4jUtil.log('info', 'MappingId exists');
    const fileStream = await fs.createReadStream(file.path);
    const fileServiceName = config.fileApi.fileServiceName ? config.fileApi.fileServiceName : 'minio';
    log4jUtil.log('info', `fileServiceName:${fileServiceName}`);
    const bucketName = config.fileApi.bucketName ? config.fileApi.bucketName : 'trans';
    const newObjectResult = await createNewObjectMapping(
      fileServiceName,
      bucketName,
      file,
      mappingLevel,
      mappingId,
    );
    log4jUtil.log('info', `newObjectResult:${JSON.stringify(newObjectResult)}`);
    if (newObjectResult && newObjectResult[`${'_id'}`]) {
      const uploadResult = await uploadFileThroughFileApi(
        config.fileApi.url,
        fileServiceName,
        bucketName,
        newObjectResult[`${'_id'}`],
        file.originalname,
        fileStream,
      );
      fs.unlinkSync(`${process.env.PWD}/${file.path}`);
      log4jUtil.log('info', 'Temp file deleted');
      if (uploadResult.error) {
        log4jUtil.log('error', JSON.stringify(uploadResult.error));
        return {
          error: uploadResult.error,
        };
      }
      log4jUtil.log('info', 'Upload successfully');
      const fileUrl = await objectStorageUtil.getFileUrlThroughFileApi(
        config.fileApi.url,
        fileServiceName,
        bucketName,
        newObjectResult[`${'_id'}`],
      );
      log4jUtil.log('info', `fileUrl:${fileUrl}`);
      return {
        id: newObjectResult[`${'_id'}`],
        url: fileUrl,
      };
    }
    log4jUtil.log('info', 'newObjectResult._id not exists');
    return newObjectResult[`${'_id'}`];
  }
  return { error: 'MappingId not exists.' };
};

export default { uploadFile };
