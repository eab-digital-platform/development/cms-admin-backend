/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import ComponentMaster from '../model/componentMaster.model';
import objectStorageUtil from '../utils/objectStorage.util';
import log4jUtil from '../utils/log4j.util';
import projectController from './project.controller';
import organizationController from './organization.controller';

/**
 * @function
 * @description Create component master
 * @param {object} componentMasterContent - component master content
 * @returns {string} component master record id if success/ error message when fail
 */
const createComponentMaster = async (componentMasterContent) => {
  const newComponentMaster = new ComponentMaster(componentMasterContent);
  const result = await newComponentMaster.save();
  return result;
};

/**
 * @function
 * @description Update a component master record
 * @param {string} componentMasterId - component master id in mongodb
 * @param {object} newComponentMasterContent - new component master content
 */
const updateComponentMaster = async (componentMasterId, newComponentMasterContent) => {
  const result = await ComponentMaster.findById(componentMasterId);
  if (result) {
    await ComponentMaster.findOneAndUpdate({ _id: componentMasterId }, newComponentMasterContent);
  }
};

/**
 * @function
 * @description Find the component master record by id
 * @param {string} componentMasterId - component master id in mongodb
 * @returns {string} The component master record
 */
const getComponentMaster = async (componentMasterId) => {
  const result = await ComponentMaster.findById(componentMasterId);
  return result;
};

const getImageUrl = async (bucket, key) => new Promise((resolve) => {
  objectStorageUtil.getObjectUrl(bucket, key).then((result) => {
    resolve(result);
  });
});

const convertDisplayImageToImageUrl = async (item) => {
  let tempItem = _.cloneDeep(item);
  tempItem = _.omit(tempItem, 'displayImage');
  tempItem.imageUrl = await getImageUrl(item.displayImage.bucket, item.displayImage.key);
  return tempItem;
};

const convertDisplayImagesToImageUrls = async (items) => {
  const resultWithImageUrl = [];
  for (let i = 0; i < items.length; i += 1) {
    if (items[`${i}`].displayImage && items[`${i}`].displayImage.bucket && items[`${i}`].displayImage.key) {
      /* eslint-disable no-await-in-loop */
      const newItem = await convertDisplayImageToImageUrl(items[`${i}`]);
      resultWithImageUrl.push(newItem);
    } else {
      resultWithImageUrl.push(items[`${i}`]);
    }
  }
  return resultWithImageUrl;
};

/**
 * @function
 * @description Get all component master data on componentMaster collection
 * @returns {array} Array of json files
 */
const getList = async () => {
  const result = await ComponentMaster.find().lean();
  log4jUtil.log('info', `List result length:${result.length}`);
  const resultWithImageUrl = await convertDisplayImagesToImageUrls(result);
  return resultWithImageUrl;
};

const assignPagesToPageSelector = async (components, pages) => {
  const componentTemp = _.cloneDeep(components);
  _.forEach(components, (component, componentIndex) => {
    _.forEach(component.style, (style, styleIndex) => {
      if (style && style.value && style.value.type && style.value.type === 'pageSelector') {
        componentTemp[`${componentIndex}`].style[`${styleIndex}`].value.options = pages;
      }
    });
    _.forEach(component.cmsProps, (cmsProps, cmsPropsIndex) => {
      if (cmsProps && cmsProps.value && cmsProps.value.type && cmsProps.value.type === 'pageSelector') {
        componentTemp[`${componentIndex}`].cmsProps[`${cmsPropsIndex}`].value.options = pages;
      }
    });
    _.forEach(component.muiPros, (muiPros, muiPropsIndex) => {
      if (muiPros && muiPros.value && muiPros.value.type && muiPros.value.type === 'pageSelector') {
        componentTemp[`${componentIndex}`].muiPros[`${muiPropsIndex}`].value.options = pages;
      }
    });
  });
  return componentTemp;
};

const getListByProjectId = async (projectId) => {
  const components = await getList();
  const pagesUnderProject = await projectController.getPageListByProjectId(projectId);
  const pagesUnderOrganization = await organizationController.getPagesByOrganizationId('');
  const pages = [...pagesUnderProject, ...pagesUnderOrganization];
  _.forEach(pages, (page, pageIndex) => {
    pages[`${pageIndex}`] = _.pick(page, ['_id', 'code']);
  });
  const result = await assignPagesToPageSelector(components, pages);
  return result;
};

const getListByOrganizationId = async (organizationId) => {
  const components = await getList();
  const pages = await organizationController.getPagesByOrganizationId(organizationId);
  _.forEach(pages, (page, pageIndex) => {
    pages[`${pageIndex}`] = _.pick(page, ['_id', 'code']);
  });
  return assignPagesToPageSelector(components, pages);
};

export default {
  createComponentMaster,
  updateComponentMaster,
  getComponentMaster,
  getList,
  getListByProjectId,
  getListByOrganizationId,
};
