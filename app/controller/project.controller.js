/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import moment from 'moment';
import Project from '../model/project.model';
import Mapping from '../model/mapping.model';
import PageMaster from '../model/pageMaster.model';
import pageMasterController from './pageMaster.controller';

/**
 * @function
 * @description Get all page master data on pageMaster collection
 * @returns {array} Array of json files
 */
const getList = async () => {
  const results = await Project.find().sort({
    'system.updatedDate': -1,
  }).lean();
  _.forEach(results, (result, i) => {
    results[`${i}`].system.createdDate = moment(results[`${i}`].system.createdDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
    results[`${i}`].system.updatedDate = moment(results[`${i}`].system.updatedDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
  });
  return results;
};

const addProject = async (project) => {
  const newProject = new Project(project);
  const result = await newProject.save();
  const id = '_id';
  return result[`${id}`];
};

const getAllPagesMasterDetailByIds = async (ids) => {
  const promises = [];
  _.forEach(ids, (id) => {
    promises.push(new Promise((resolve) => {
      PageMaster.findById(id).lean().then((result) => {
        _.set(result, 'system.updatedDate', moment(result.system.updatedDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss'));
        _.set(result, 'system.createdDate', moment(result.system.createdDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss'));
        resolve(result);
      });
    }));
  });
  let finalResult = [];
  let finalError = {};
  await Promise.all(promises).then((result) => {
    finalResult = result;
  }).catch((e) => {
    finalError = e;
  });
  if (!_.isEmpty(finalError)) {
    return finalError;
  }
  return _.sortBy(finalResult, 'system.updatedDate');
};

const getPageListByProjectId = async (projectId) => {
  const mapping = await Mapping.find({
    mappingId: projectId,
    mappingLevel: 'project',
    ownerLevel: 'pageMaster',
  }).lean();
  const ownerIdArray = _.map(mapping, 'ownerId');
  const finalResult = await getAllPagesMasterDetailByIds(ownerIdArray);
  return _.sortBy(finalResult, 'code');
};

const deleteProject = async (projectId) => {
  const existingProject = await Project.findById(projectId);
  if (!existingProject) {
    return {
      error: 'Project id not exist',
    };
  }
  await Project.findByIdAndDelete(projectId);
  return {};
};

const updateProject = async (projectId, project) => {
  const existingProject = await Project.findById(projectId);
  if (!existingProject) {
    return {
      error: 'Project id not exist',
    };
  }
  const newProject = _.cloneDeep(project);
  await Project.findByIdAndUpdate(projectId, newProject);
  return {};
};

const getProject = async (projectId) => {
  const project = await Project.findById(projectId);
  return project;
};

const clonePagesAndApplyMapping = async (projectId, pages) => {
  const promises = [];
  _.forEach(pages, (page) => {
    const clonedPage = _.cloneDeep(page);
    clonedPage.system.createdDate = moment();
    clonedPage.system.updatedDate = moment();
    const newPage = new PageMaster(_.omit(clonedPage, ['_id']));
    promises.push(pageMasterController.createPageMasterByProjectId(projectId, newPage));
  });
  const result = await Promise.all(promises);
  return result;
};

const cloneProject = async (projectId) => {
  const oldProject = await Project.findById(projectId).lean();
  if (!oldProject) {
    return {
      error: 'project not exists',
    };
  }
  const clonedProject = _.cloneDeep(oldProject);
  clonedProject.title.en = `${oldProject.title.en} Copy`;
  clonedProject.system.createdDate = moment();
  clonedProject.system.updatedDate = moment();
  const newProject = new Project(_.omit(clonedProject, ['_id']));
  const newSavedProject = await newProject.save();
  const pagesUnderCurrentProject = await getPageListByProjectId(projectId);
  await clonePagesAndApplyMapping(newSavedProject[`${'_id'}`], pagesUnderCurrentProject);
  return newSavedProject[`${'_id'}`];
};

export default {
  getList,
  addProject,
  getPageListByProjectId,
  deleteProject,
  updateProject,
  getProject,
  cloneProject,
};
