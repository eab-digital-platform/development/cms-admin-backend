/**
 * @module controller/jsonStore
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 */
import * as _ from 'lodash';
import moment from 'moment';
import ComponentSet from '../model/componentSet.model';
import Project from '../model/project.model';
import Organization from '../model/organization.model';
import Mapping from '../model/mapping.model';

/**
 * @function
 * @description Create component master
 * @param {object} componentMasterContent - component master content
 * @returns {string} component master record id if success/ error message when fail
 */
const createComponentSet = async (componentSetContent) => {
  const newComponentSet = new ComponentSet(componentSetContent);
  const result = await newComponentSet.save();
  return result;
};

/**
 * @function
 * @description Update a component master record
 * @param {string} componentMasterId - component master id in mongodb
 * @param {object} newComponentMasterContent - new component master content
 */
const updateComponentSet = async (componentSetId, newComponentSetContent) => {
  const result = await ComponentSet.findById(componentSetId);
  if (result) {
    await ComponentSet.findOneAndUpdate({ _id: componentSetId }, newComponentSetContent);
  }
};

/**
 * @function
 * @description Find the component master record by id
 * @param {string} componentMasterId - component master id in mongodb
 * @returns {string} The component master record
 */
const getComponentSetById = async (componentSetId) => {
  const result = await ComponentSet.findById(componentSetId);
  return result;
};

const getAllComponentSetDetailByIds = async (ids) => {
  const promises = [];
  _.forEach(ids, (id) => {
    promises.push(new Promise((resolve) => {
      ComponentSet.findById(id).lean().then((result) => {
        _.set(result, 'system.updatedDate', moment(result.system.updatedDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss'));
        _.set(result, 'system.createdDate', moment(result.system.createdDate).add(8, 'hours').format('YYYY-MM-DD HH:mm:ss'));
        resolve(result);
      });
    }));
  });
  let finalResult = [];
  let finalError = {};
  await Promise.all(promises).then((result) => {
    finalResult = result;
  }).catch((e) => {
    finalError = e;
  });
  if (!_.isEmpty(finalError)) {
    return finalError;
  }
  return _.sortBy(finalResult, 'system.updatedDate');
};

const getListByOrganizationId = async (organizationId) => {
  const mapping = await Mapping.find({
    mappingId: organizationId,
    mappingLevel: 'organization',
    ownerLevel: 'componentSet',
  }).lean();
  const ownerIdArray = _.map(mapping, 'ownerId');
  const finalResult = await getAllComponentSetDetailByIds(ownerIdArray);
  return _.sortBy(finalResult, 'system.updatedDate').reverse();
};

const getListByProjectId = async (projectId) => {
  const mapping = await Mapping.find({
    mappingId: projectId,
    mappingLevel: 'project',
    ownerLevel: 'componentSet',
  }).lean();
  const ownerIdArray = _.map(mapping, 'ownerId');
  const finalResult = await getAllComponentSetDetailByIds(ownerIdArray);
  return _.sortBy(finalResult, 'system.updatedDate').reverse();
};

const createComponentSetByProjectId = async (projectId, newComponentSet) => {
  const existingProject = await Project.findById(projectId);
  if (!existingProject) {
    return {
      error: 'Project not exists',
    };
  }
  const newComponentSetRecord = new ComponentSet(newComponentSet);
  const result = await newComponentSetRecord.save();
  const id = '_id';
  const newMapping = new Mapping({
    ownerLevel: 'componentSet',
    ownerId: result[`${id}`],
    mappingLevel: 'project',
    mappingId: projectId,
  });
  await newMapping.save();
  return result[`${id}`];
};

const createComponentSetByOrganizationId = async (organizationId, newComponentSet) => {
  const existingOrganization = await Organization.findById(organizationId);
  if (!existingOrganization) {
    return {
      error: 'Organization not exists',
    };
  }
  const newComponentSetRecord = new ComponentSet(newComponentSet);
  const result = await newComponentSetRecord.save();
  const id = '_id';
  const newMapping = new Mapping({
    ownerLevel: 'componentSet',
    ownerId: result[`${id}`],
    mappingLevel: 'organization',
    mappingId: organizationId,
  });
  await newMapping.save();
  return result[`${id}`];
};

export default {
  createComponentSet,
  createComponentSetByProjectId,
  createComponentSetByOrganizationId,
  updateComponentSet,
  getComponentSetById,
  getListByOrganizationId,
  getListByProjectId,
};
