import { generalNotFoundResponse } from '../utils/response.util';
import componentMasterRoute from '../routes/componentMaster.route';
import pageMasterRoute from '../routes/pageMaster.route';
import testRouter from '../routes/test.route';
import pageTemplateRoute from '../routes/pageTemplate.route';
import projectRoute from '../routes/project.route';
import organizationRoute from '../routes/organization.route';
import componentSetRoute from '../routes/componentSet.route';

const loadRouterMiddleware = (app) => {
  app.use('/componentMaster', componentMasterRoute);
  app.use('/pageMaster', pageMasterRoute);
  app.use('/pageTemplate', pageTemplateRoute);
  app.use('/project', projectRoute);
  app.use('/organization', organizationRoute);
  app.use('/test', testRouter);
  app.use('/componentSet', componentSetRoute);
  // 404 handler
  app.use((req, res) => {
    generalNotFoundResponse(res);
  });
};

export default loadRouterMiddleware;
