import objectStorageUtil from '../utils/objectStorage.util';

const loadObjectStorageMiddleware = async () => {
  objectStorageUtil.connectObjectStorage();
  // objectStorageUtil.getObjectUrl('cms-admin-components-images', 'Tab (Horizontal).svg');
};

export default loadObjectStorageMiddleware;
