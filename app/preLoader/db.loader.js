import mongoose from 'mongoose';

import getConfig from '../utils/config.util';
import log4jUtil from '../utils/log4j.util';

const connectDB = () => {
  if (
    getConfig('mongodb.url')
    && getConfig('mongodb.port')
    && getConfig('mongodb.dbName')
    && getConfig('mongodb.userName')
    && getConfig('mongodb.password')
    && !!getConfig('mongodb.reconnectTries')
  ) {
    mongoose.connect(`mongodb://${getConfig('mongodb.url')}:${getConfig('mongodb.port')}/${getConfig('mongodb.dbName')}`, {
      useNewUrlParser: true,
      auth: {
        user: getConfig('mongodb.userName'),
        password: getConfig('mongodb.password'),
      },
      useUnifiedTopology: true,
    });
    mongoose.set('useFindAndModify', false);
    mongoose.connection.on('connected', () => {
      log4jUtil.log('info', `Connected to Mongodb: ${getConfig('mongodb.url')}:${getConfig('mongodb.port')} [${getConfig('mongodb.dbName')}]`);
    });
    mongoose.connection.on('disconnecting', () => {
      log4jUtil.log('error', 'Disconnecting to Mongodb');
    });
    mongoose.connection.on('reconnected', () => {
      log4jUtil.log('info', 'Mongodb reconnected.');
    });
    mongoose.connection.on('error', (err) => {
      log4jUtil.log('error', err);
    });
  }
  return '';
};

export default connectDB;
