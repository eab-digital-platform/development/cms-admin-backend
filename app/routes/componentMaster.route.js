import express from 'express';

import componentMasterController from '../controller/componentMaster.controller';
import { generalSuccessResponse } from '../utils/response.util';

const router = express.Router();

router.post('/', async (req, res) => {
  await componentMasterController.createComponentMaster(req.body);
  generalSuccessResponse(res, null);
});

router.put('/id/:componentMasterId', async (req, res) => {
  await componentMasterController.updateComponentMaster(req.params.componentMasterId, req.body);
  generalSuccessResponse(res, null);
});

router.get('/id/:componentMasterId', async (req, res) => {
  if (req.params.componentMasterId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await componentMasterController.getComponentMaster(req.params.componentMasterId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.delete('/id/:componentMasterId', async (req, res) => {
  if (req.params.componentMasterId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await componentMasterController
      .deleteComponentMaster(req.params.componentMasterId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.get('/list', async (req, res) => {
  const result = await componentMasterController.getList();
  generalSuccessResponse(res, result);
});

export default router;
