import express from 'express';
import multer from 'multer';

import pageMasterController from '../controller/pageMaster.controller';
import objectMappingController from '../controller/objectMapping.controller';
import { generalSuccessResponse, generalBadRequestResponse } from '../utils/response.util';

const upload = multer({ dest: 'uploads/' });

const router = express.Router();

router.post('/', async (req, res) => {
  const result = await pageMasterController.createPageMaster(req.body);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.put('/id/:pageMasterId', async (req, res) => {
  const result = await pageMasterController.updatePageMaster(req.params.pageMasterId, req.body);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, null);
  }
});

router.get('/id/:pageMasterId', async (req, res) => {
  if (req.params.pageMasterId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await pageMasterController.getPageMaster(req.params.pageMasterId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.delete('/id/:pageMasterId', async (req, res) => {
  if (req.params.pageMasterId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await pageMasterController.deletePageMaster(req.params.pageMasterId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.get('/list', async (req, res) => {
  const result = await pageMasterController.getList();
  generalSuccessResponse(res, result);
});

router.post('/:pageMasterId/clone', async (req, res) => {
  const result = await pageMasterController.clonePageMaster(req.params.pageMasterId);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.post('/:pageMasterId/move', async (req, res) => {
  if (req.body.level === 'organization' && req.body.targetId) {
    await pageMasterController.movePage(req.params.pageMasterId, 'organization', req.body.targetId);
    generalSuccessResponse(res, null);
  } else if (req.body.level === 'project' && req.body.targetId) {
    await pageMasterController.movePage(req.params.pageMasterId, 'project', req.body.targetId);
    generalSuccessResponse(res, null);
  } else {
    generalBadRequestResponse(res, 'level incorrect');
  }
});

router.post('/:pageMasterId/upload', upload.single('file'), async (req, res) => {
  if (req.file) {
    const result = await objectMappingController.uploadFile(req.file, 'pageMaster', req.params.pageMasterId);
    if (result.error) {
      generalBadRequestResponse(res, result);
    } else {
      generalSuccessResponse(res, result);
    }
  } else {
    generalBadRequestResponse(res, 'File not exists');
  }
});

router.get('/:pageMasterId/object/:objectId', async (req, res) => {
  const result = await pageMasterController.getObjectInfo(
    req.params.pageMasterId,
    req.params.objectId,
  );
  generalSuccessResponse(res, result);
});

export default router;
