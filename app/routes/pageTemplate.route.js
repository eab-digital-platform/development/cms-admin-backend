import express from 'express';

import pageTemplateController from '../controller/pageTemplate.controller';
import { generalSuccessResponse } from '../utils/response.util';

const router = express.Router();

router.get('/id/:pageTemplateId', async (req, res) => {
  if (req.params.pageTemplateId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await pageTemplateController.getPageTemplate(req.params.pageTemplateId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.get('/list', async (req, res) => {
  const result = await pageTemplateController.getList();
  generalSuccessResponse(res, result);
});

export default router;
