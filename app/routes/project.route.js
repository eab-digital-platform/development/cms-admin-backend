import express from 'express';

import projectController from '../controller/project.controller';
import {
  generalSuccessResponse,
  generalBadRequestResponse,
  generalInternalServerErrorResponse,
} from '../utils/response.util';
import pageMasterController from '../controller/pageMaster.controller';
import componentSetController from '../controller/componentSet.controller';
import componentMasterController from '../controller/componentMaster.controller';

const router = express.Router();

router.get('/list', async (req, res) => {
  const result = await projectController.getList();
  generalSuccessResponse(res, result);
});

router.post('/', async (req, res) => {
  const result = await projectController.addProject(req.body);
  if (result.error) {
    generalBadRequestResponse(res, result);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.get('/:projectId/page/list', async (req, res) => {
  const result = await projectController.getPageListByProjectId(req.params.projectId);
  generalSuccessResponse(res, result);
});

router.delete('/:projectId', async (req, res) => {
  const result = await projectController.deleteProject(req.params.projectId);
  if (result.error) {
    generalBadRequestResponse(res, result);
  } else {
    generalSuccessResponse(res, null);
  }
});

router.put('/:projectId', async (req, res) => {
  const result = await projectController.updateProject(req.params.projectId, req.body);
  if (result.error) {
    generalBadRequestResponse(res, result);
  } else {
    generalSuccessResponse(res, null);
  }
});

router.get('/:projectId', async (req, res) => {
  const result = await projectController.getProject(req.params.projectId);
  generalSuccessResponse(res, result);
});

router.post('/:projectId/clone', async (req, res) => {
  const newProjectId = await projectController.cloneProject(req.params.projectId);
  generalSuccessResponse(res, newProjectId);
});

router.post('/:projectId/pageMaster', async (req, res) => {
  const result = await pageMasterController.createPageMasterByProjectId(
    req.params.projectId,
    req.body,
  );
  if (result.error) {
    generalInternalServerErrorResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.post('/:projectId/componentSet', async (req, res) => {
  const result = await componentSetController.createComponentSetByProjectId(
    req.params.projectId,
    req.body,
  );
  if (result.error) {
    generalInternalServerErrorResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.get('/:projectId/componentSet/list', async (req, res) => {
  const result = await componentSetController.getListByProjectId(req.params.projectId);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.get('/:projectId/componentMaster/list', async (req, res) => {
  const result = await componentMasterController.getListByProjectId(req.params.projectId);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

export default router;
