import express from 'express';
import multer from 'multer';
import componentSetController from '../controller/componentSet.controller';
import objectMappingController from '../controller/objectMapping.controller';
import { generalSuccessResponse, generalBadRequestResponse } from '../utils/response.util';

const router = express.Router();

const upload = multer({ dest: 'uploads/' });

router.post('/', async (req, res) => {
  await componentSetController.createComponentSet(req.body);
  generalSuccessResponse(res, null);
});

router.put('/:componentSetId', async (req, res) => {
  await componentSetController.updateComponentSet(req.params.componentSetId, req.body);
  generalSuccessResponse(res, null);
});

router.get('/:componentSetId', async (req, res) => {
  if (req.params.componentSetId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await componentSetController.getComponentSetById(req.params.componentSetId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.delete('/:componentSetId', async (req, res) => {
  if (req.params.componentSetId.match(/^[0-9a-fA-F]{24}$/)) {
    const result = await componentSetController.deleteComponentSet(req.params.componentSetId);
    generalSuccessResponse(res, result);
  } else {
    res.sendStatus(400);
  }
});

router.get('/list', async (req, res) => {
  const result = await componentSetController.getList();
  generalSuccessResponse(res, result);
});

router.post('/:componentSetId/upload', upload.single('file'), async (req, res) => {
  if (req.file) {
    const result = await objectMappingController.uploadFile(req.file, 'componentSet', req.params.componentSetId);
    generalSuccessResponse(res, result);
  } else {
    generalBadRequestResponse(res, 'File not exists');
  }
});

export default router;
