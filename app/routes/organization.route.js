import express from 'express';

import organizationController from '../controller/organization.controller';
import componentSetController from '../controller/componentSet.controller';
import { generalSuccessResponse, generalBadRequestResponse } from '../utils/response.util';
import pageMasterController from '../controller/pageMaster.controller';
import componentMasterController from '../controller/componentMaster.controller';

const router = express.Router();

router.get('/:organizationId/page/list', async (req, res) => {
  const result = await organizationController.getPagesByOrganizationId(req.params.organizationId);
  generalSuccessResponse(res, result);
});

router.get('/:organizationId', async (req, res) => {
  const result = await organizationController.getOrganization(req.params.organizationId);
  generalSuccessResponse(res, result);
});

router.post('/:organizationId/componentSet', async (req, res) => {
  const result = await componentSetController
    .createComponentSetByOrganizationId(req.params.organizationId, req.body);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.post('/:organizationId/pageMaster', async (req, res) => {
  const result = await pageMasterController
    .createPageMasterByOrganizationId(req.params.organizationId, req.body);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.get('/:organizationId/componentSet/list', async (req, res) => {
  const result = await componentSetController.getListByOrganizationId(req.params.organizationId);
  generalSuccessResponse(res, result);
});

router.get('/:organizationId/componentMaster/list', async (req, res) => {
  const result = await componentMasterController.getListByOrganizationId(req.params.organizationId);
  generalSuccessResponse(res, result);
});

export default router;
