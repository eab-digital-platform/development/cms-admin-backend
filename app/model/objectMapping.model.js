const mongoose = require('mongoose');

const objectMappingModel = new mongoose.Schema({
  fileServiceName: {
    type: String,
    default: 'minio',
  },
  bucketName: {
    type: String,
  },
  meta: {
    type: Object,
  },
  mappingLevel: {
    type: String,
  },
  mappingId: {
    type: String,
  },
  system: {
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'objectMapping' });

module.exports = mongoose.model('objectMapping', objectMappingModel);
