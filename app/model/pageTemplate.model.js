const mongoose = require('mongoose');

const pageTemplateModel = new mongoose.Schema({
  cmsComponentType: {
    type: String,
    required: true,
  },
  style: {
    type: Object,
    required: true,
    default: {},
  },
  cmsProps: {
    type: Object,
    default: {},
  },
  muiProps: {
    type: Object,
    default: {},
  },
  items: {
    type: Array,
    required: false,
  },
  displayName: {
    type: Object,
    default: {},
  },
  displayImage: {
    type: Object,
    default: {
      bucket: String,
      key: String,
    },
  },
  system: {
    userComponent: {
      type: Boolean,
      required: true,
      default: false,
    },
    group: {
      type: String,
      required: true,
      default: 'inputs',
    },
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'pageTemplate' });

module.exports = mongoose.model('pageTemplate', pageTemplateModel);
