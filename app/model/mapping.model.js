const mongoose = require('mongoose');

const mappingModel = new mongoose.Schema({
  ownerLevel: {
    type: String,
    required: true,
  },
  ownerId: {
    type: String,
    required: true,
  },
  mappingLevel: {
    type: String,
    required: true,
  },
  mappingId: {
    type: String,
    required: true,
  },
  system: {
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'mapping' });

module.exports = mongoose.model('mapping', mappingModel);
