const mongoose = require('mongoose');

const pageMasterModel = new mongoose.Schema({
  code: {
    type: String,
    required: true,
  },
  level: {
    type: String,
    required: true,
  },
  pageId: {
    type: String,
    required: true,
  },
  description: {
    type: Object,
    required: false,
  },
  cmsComponentType: {
    type: String,
  },
  style: {
    type: Object,
    required: true,
    default: {},
  },
  cmsProps: {
    type: Object,
    required: true,
    default: {},
  },
  muiProps: {
    type: Object,
    required: true,
    default: {},
  },
  items: {
    type: Object,
    required: false,
  },
  system: {
    userComponent: {
      type: Boolean,
      required: true,
      default: false,
    },
    group: {
      type: String,
      required: true,
      default: 'inputs',
    },
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'pageMaster' });

module.exports = mongoose.model('pageMaster', pageMasterModel);
