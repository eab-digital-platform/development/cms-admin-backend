const mongoose = require('mongoose');

const componentSetModel = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  cmsComponentType: {
    type: String,
    required: false,
  },
  style: {
    type: Object,
    required: false,
    default: {},
  },
  cmsProps: [{
    key: String,
    value: {
      title: String,
      type: String,
    },
    required: false,
  }],
  muiProps: {
    type: Object,
    required: false,
    default: {},
  },
  items: {
    type: Array,
    required: false,
  },
  system: {
    createdDate: {
      type: Date,
      default: Date.now,
      required: false,
    },
    createdBy: {
      type: String,
      required: false,
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: false,
    },
    updatedBy: {
      type: String,
      required: false,
    },
  },
}, { collection: 'componentSet' });

module.exports = mongoose.model('componentSet', componentSetModel);
