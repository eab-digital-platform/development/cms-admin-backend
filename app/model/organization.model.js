const mongoose = require('mongoose');

const organizationModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  system: {
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'organization' });

module.exports = mongoose.model('organization', organizationModel);
