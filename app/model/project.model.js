const mongoose = require('mongoose');

const projectModel = new mongoose.Schema({
  title: {
    type: Object,
    default: {},
  },
  description: {
    type: Object,
    default: {},
  },
  settings: {
    theme: {
      type: String,
      default: 'H1-Theme',
      required: true,
    },
    environmentVariableSetting: [{
      name: {
        type: String,
      },
      value: {
        type: String,
      },
    }],
    firstPageId: {
      type: String,
    },
  },
  system: {
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
      default: 'admin',
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
      default: 'admin',
    },
  },
}, { collection: 'project' });

module.exports = mongoose.model('project', projectModel);
