const mongoose = require('mongoose');

const componentMasterModel = new mongoose.Schema({
  isProtected: {
    type: Boolean,
  },
  description: {
    type: Object,
    required: false,
  },
  displayName: {
    type: Object,
    default: {},
  },
  displayImage: {
    type: Object,
    default: {
      bucket: String,
      key: String,
    },
  },
  cmsComponentType: {
    type: String,
    required: true,
  },
  style: {
    type: Object,
    default: {},
  },
  cmsProps: {
    type: Object,
    default: {},
  },
  muiProps: {
    type: Object,
    default: {},
  },
  system: {
    userComponent: {
      type: Boolean,
      required: true,
    },
    group: {
      type: String,
      required: true,
    },
    createdDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    createdBy: {
      type: String,
      required: true,
    },
    updatedDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    updatedBy: {
      type: String,
      required: true,
    },
  },
  items: {
    type: Array,
    required: false,
  },
}, { collection: 'componentMaster' });

module.exports = mongoose.model('componentMaster', componentMasterModel);
