FROM node:12.2.0-alpine
EXPOSE 3000

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY . /app

RUN npm install --loglevel error

# start app

#RUN npm start
CMD ["npm","run", "dev"]
